<%-- 
    Document   : salida
    Created on : 05-abr-2020, 15:26:25
    Author     : espin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>Ejercicio Nº1</title>
    </head>
   <body>
        <nav class="navbar navbar-dark bg-dark">
            <span class="text-justify" style="color: #ffffff" >Calculadora de Interés Simple</span>
        </nav>
        <div class="container">
            <%
            int resultado = (int)request.getSession().getAttribute("resultado");
            
            %>
            
            <h1>¡Enhorabuena!</h1>
            <h2> El ínteres generado es de : <%=resultado%></h2>
            <a href="index.jsp"><input type="submit" value="Recalcular" name="Volver" /></a>
        </div>
    </body>
</html>
