/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author espin
 */
public class Calcula {
    public double capital;
    public double taza;
    public double plazo;
    public double resultado;
       
    public double getCalculo(String capital, String taza, String plazo){
    
        this.capital = Double.parseDouble(capital);
        this.taza = Double.parseDouble(taza);
        this.plazo = Double.parseDouble(plazo);
        
        this.resultado = this.capital*(this.taza/100)*this.plazo;
        
        return this.resultado;
    }
}
