<%-- 
    Document   : index
    Created on : 05-abr-2020, 15:19:29
    Author     : espin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>Ejercicio Nº1</title>
    </head>
    <body>     
        <nav class="navbar navbar-dark bg-dark">
            <span class="text-justify" style="color: #ffffff" >Calculadora de Interés Simple</span>
        </nav>
        <div class="container">
        <form name="form" action="procesar.do" method="POST">
            <div class="form-group">
            <label>Capital Inicial</label>
            <input type="number" class="form-control" name="capital" >
            </div>
            
            <div class="form-group">
            <label>Tasa de Interés (%)</label>
            <input type="number" class="form-control" name="tasa" >
            </div> 
            
            <div class="form-group">
            <label>Plazo en años</label>
            <input type="number" class="form-control" name="plazo" >
            </div>
            
            <input type="submit" value="Calcular"/>
        </form>
        </div>
        
    </body>
</html>